<?php

namespace Dreamsoft\Order\vServerBasic;

use App\Helpers\Plugin;
use Illuminate\Support\ServiceProvider;

class VServerServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/views', 'ds_vserver_basic');
        $this->loadRoutesFrom(__DIR__ . '/routes/web.php');
        \App\Helpers\PackageHelper::addMenuEntry('vServer', 'order/vserver', 'vserver.ds.order.basic');

        //GENERATE SETTINGS
        \App\Helpers\SettingHelper::addSettingEntry('vserver.systems', 'Liste der Betriebssysteme', [['name' => 'Ubuntu 14.04', 'file' => 'ubuntu-14.04-standard_14.04-1_amd64.tar.gz']], 'vserver.ds.order.basic', 'json');
        \App\Helpers\SettingHelper::addSettingEntry('vserver.lifetimes', 'Laufzeiten', [['days' => 1, 'discount' => -15], ['days' => 7, 'discount' => -10]], 'vserver.ds.order.basic', 'json');
        \App\Helpers\SettingHelper::addSettingEntry('vserver.storages', 'Festplattengröße in HDD', [
            ['size' => 10, 'price' => 0.10],
            ['size' => 25, 'price' => 0.25],
            ['size' => 50, 'price' => 0.50],
        ], 'vserver.ds.order.basic', 'json');
        \App\Helpers\SettingHelper::addSettingEntry('vserver.cores', 'CPU-Kerne', [
            ['size' => 1, 'price' => 1],
            ['size' => 2, 'price' => 2],
            ['size' => 3, 'price' => 3],
        ], 'vserver.ds.order.basic', 'json');
        \App\Helpers\SettingHelper::addSettingEntry('vserver.ram', 'Arbeitsspeicher in MB', [
            ['size' => 512, 'price' => 0.50],
            ['size' => 1024, 'price' => 1],
            ['size' => 2048, 'price' => 2],
        ], 'vserver.ds.order.basic', 'json');
    }

    public function register()
    {
    }
}
