<?php

Route::group(['middleware' => 'web'], function () {
    Route::get('order/vserver', function () {
        return view('ds_vserver_basic::vserver');
    });

    Route::post('order/vserver', 'Dreamsoft\Order\vServerBasic\vServerController@addToShoppingCart');

});