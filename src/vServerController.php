<?php

namespace Dreamsoft\Order\vServerBasic;

use App\Helpers\Plugin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class vServerController extends Controller
{
    public function addToShoppingCart(Request $request)
    {
        $discount = 0.00;
        $price = 0.00;

        //STORAGE
        foreach (\App\Helpers\SettingHelper::getSetting('vserver.storages') AS $item) {
            if ($item->size == $request->input('storage')) {
                $price += $item->price;
                break;
            }
        }

        //Cores
        foreach (\App\Helpers\SettingHelper::getSetting('vserver.cores') AS $item) {
            if ($item->size == $request->input('cores')) {
                $price += $item->price;
                break;
            }
        }

        //RAM
        foreach (\App\Helpers\SettingHelper::getSetting('vserver.ram') AS $item) {
            if ($item->size == $request->input('ram')) {
                $price += $item->price;
                break;
            }
        }

        //Lifetime
        foreach (\App\Helpers\SettingHelper::getSetting('vserver.lifetimes') AS $item) {
            if ($item->days == $request->input('lifetime')) {
                $discount = $item->discount;
                break;
            }
        }

        $price = ($price / 100) * (100 - $discount);
        $price = round($price, 2);

        $configuration = [
            'image' => $request->input('image'),
            'lifetime' => $request->input('lifetime'),
            'storage' => $request->input('storage'),
            'cores' => $request->input('cores'),
            'ram' => $request->input('ram'),
        ];

        $systemName = '';

        foreach (\App\Helpers\SettingHelper::getSetting('vserver.systems') AS $item) {
            if ($item->file == $request->input('image')) {
                $systemName = $item->name;
                break;
            }
        }

        $name = 'Prepaid vServer';
        $text = 'Laufzeit: ' . $request->input('lifetime') . ' Tage' . PHP_EOL;
        $text .= 'Betriebssystem: ' . $systemName . PHP_EOL;
        $text .= 'Speicherplatz: ' . $request->input('storae') . ' GB' . PHP_EOL;
        $text .= 'Kerne: ' . $request->input('cores') . PHP_EOL;
        $text .= 'RAM: ' . $request->input('ram') . ' MB' . PHP_EOL;

        $save = \App\Helpers\PackageHelper::addItemToShoppingCart($name, $configuration, $text, $price, 'vserver.ds.order.basic', 'vserver.lxc');

        if ($save) {
            return redirect(url('shopping_cart'))->with('success', 'Der Artikel ' . $name . ' wurde deinem Einkaufswagen hinzugefügt.');
        }
    }
}
