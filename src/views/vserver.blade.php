@extends(env('TEMPLATE').'::frontend.app')

@section('content')
    <div class="container">
        <div class="col-sm-10">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">vServer bestellen</h3>
                </div>
                <div class="panel-body">

                    <form class="form-horizontal" method="POST" action="{{ url('order/vserver') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="_method" value="POST">

                        <div class="form-group">
                            <label for="image" class="col-sm-2 control-label">Betriebssystem</label>
                            <div class="col-sm-10">
                                <select name="image" id="image" class="form-control">
                                    @foreach(\App\Helpers\SettingHelper::getSetting('vserver.systems') AS $item)
                                        <option value="{{ $item->file }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="lifetime" class="col-sm-2 control-label">Laufzeit</label>
                            <div class="col-sm-10">
                                <select name="lifetime" id="lifetime" class="form-control" onchange="calculatePrice();">
                                    @foreach(\App\Helpers\SettingHelper::getSetting('vserver.lifetimes') AS $item)
                                        <option value="{{ $item->days }}"
                                                data-discount="{{ $item->discount }}">{{ $item->days }} Tage
                                            @if($item->discount > 0.00)
                                                ({{ $item->discount }}% Rabatt)
                                            @else
                                                ({{ $item->discount*-1 }}% Gebühr)
                                            @endif
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="storage" class="col-sm-2 control-label">Speicherplatz</label>
                            <div class="col-sm-10">
                                <select name="storage" id="storage" class="form-control" onchange="calculatePrice();">
                                    @foreach(\App\Helpers\SettingHelper::getSetting('vserver.storages') AS $item)
                                        <option value="{{ $item->size }}"
                                                data-price="{{ $item->price }}">{{ $item->size }} GB
                                            ({{ $item->price }}&euro;)
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="storage" class="col-sm-2 control-label">Kerne</label>
                            <div class="col-sm-10">
                                <select name="cores" id="cores" class="form-control" onchange="calculatePrice();">
                                    @foreach(\App\Helpers\SettingHelper::getSetting('vserver.cores') AS $item)
                                        <option value="{{ $item->size }}"
                                                data-price="{{ $item->price }}">{{ $item->size }}
                                            ({{ $item->price }}&euro;)
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="storage" class="col-sm-2 control-label">Ram</label>
                            <div class="col-sm-10">
                                <select name="ram" id="ram" class="form-control" onchange="calculatePrice();">
                                    @foreach(\App\Helpers\SettingHelper::getSetting('vserver.ram') AS $item)
                                        <option value="{{ $item->size }}"
                                                data-price="{{ $item->price }}">{{ $item->size }} MB
                                            ({{ $item->price }}&euro;)
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="pull-right">
                                <strong>Preis für die Laufzeit: <span id="lifetime_price">0,00</span> &euro;</strong>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success btn-order pull-right"><i
                                        class="fa fa-shopping-cart"></i> in den Warenkorb packen
                            </button>
                        </div>
                    </form>

                </div>
            </div>
        </div>

        <div class="col-sm-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Kontakt</h3>
                </div>
                <div class="panel-body">
                    <i class="fa fa-phone"></i> {{ \App\Helpers\SettingHelper::getSetting('site.phone') }}<br/>
                    <i class="fa fa-envelope"></i> {{ \App\Helpers\SettingHelper::getSetting('site.mail') }}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        var calculatePrice = function () {
            var price = 0.00;

            //STORAGE
            price += parseFloat($('#storage option:selected').attr("data-price"));

            //CORES
            price += parseFloat($('#cores option:selected').attr("data-price"));

            //RAM
            price += parseFloat($('#ram option:selected').attr("data-price"));

            //LIFETIME
            var discount = parseFloat($('#lifetime option:selected').attr("data-discount"));
            var lifetime = parseFloat($('#lifetime option:selected').attr("value"));

            price = (price / 100) * (100 - discount);

            price = Math.round(price * 100) / 100;
            $('#lifetime_price').html(price.toString().replace('.', ','));
        }

        $(document).ready(function () {
            calculatePrice();
        });
    </script>
@endsection
